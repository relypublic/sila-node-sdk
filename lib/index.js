'use strict';

var _web = require('web3');

var _web2 = _interopRequireDefault(_web);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var uuid4 = require('uuid4');
var request = require('request');
var crypto = require('eth-crypto');

var appKey = null;
var appHandle = null;
var sandbox = true;
var env = 'PROD';
var baseUrl = 'https://sandbox.silamoney.com/0.2/';
var logging = false;

var web3 = new _web2.default('http://52.13.246.239:8080/');

var url = function url(path) {
  return baseUrl + path;
};
var getBalanceURL = function getBalanceURL() {
  var balanceURL = '';
  switch (env) {
    case 'PROD':
      balanceURL = sandbox ? 'https://sandbox.silatokenapi.silamoney.com/silaBalance' : 'https://silatokenapi.silamoney.com/silaBalance';
      break;
    default:
      balanceURL = 'https://test.silatokenapi.silamoney.com/silaBalance';
  }
  return balanceURL;
};

var sign = function sign(string, key) {
  if (!appKey || !key) {
    throw new Error('Unable to sign request: keys not set');
  }
  var hash = crypto.hash.keccak256(string);
  var signature = crypto.sign(key, hash);

  if (logging && env !== 'PROD') {
    console.log('*** MESSAGE STRING ***');
    console.log(string);
    console.log('*** HASH ***');
    console.log(hash);
    console.log('*** SIGNING WITH KEY ***');
    console.log(key);
    console.log('*** SIGNATURE (remove leading 0x before sending) ***');
    console.log(signature);
  }
  return signature.substr(2);
};

var configureUrl = function configureUrl() {
  var app = sandbox ? 'sandbox' : 'api';
  if (env === 'PROD') {
    baseUrl = 'https://' + app + '.silamoney.com/0.2/';
  } else {
    baseUrl = 'https://' + env.toLowerCase() + '.' + app + '.silamoney.com/0.2/';
  }
};

var signOpts = function signOpts(opts, key) {
  var options = opts;
  options.headers = {};
  options.headers.authsignature = sign(JSON.stringify(options.body), appKey);
  options.headers.usersignature = sign(JSON.stringify(options.body), key);
  return options;
};

var setHeaders = function setHeaders(msg, handle) {
  var message = msg;
  message.header.user_handle = handle;
  message.header.auth_handle = appHandle;
  message.header.reference = uuid4();
  message.header.created = Math.floor(new Date() / 1000) - 30;
  return message;
};

var template = function template(name) {
  var options = {
    url: url('getschema?schema=MessageFactory'),
    headers: {
      'Content-Type': 'text/plain',
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Max-Age': 3600
    }
  };
  return new Promise(function (res, rej) {
    request.get(options, function (err, response, body) {
      if (response === undefined || response.statusCode !== 200) {
        rej(new Error('Unable to get templates'));
        return false;
      }
      if (err){
        rej(err);
      }
      var templates = JSON.parse(body);
      var tmp = false;
      for (var i = 0; i < templates.length; i += 1) {
        if (name === templates[i].data.message) {
          tmp = templates[i].data;
          break;
        }
      }
      if (!tmp) {
        rej(new Error('Unable to find "' + name + '" in the MessageFactory'));
      } else {
        res(tmp);
      }
      return null;
    });
  });
};

var post = function post(options) {
  var promise = new Promise(function (res, rej) {
    if (logging && env !== 'PROD') {
      console.log('*** REQUEST ***');
      console.log(options.body);
    }
    request.post(options, function (err, response, body) {
      if (err) {
        rej(err);
      }
      body.statusCode = response.statusCode
      res(body);
    });
  });
  return promise;
};

var getFullHandle = function getFullHandle(handle) {
  var fullHandle = String(handle);
  if (!fullHandle.endsWith('.silamoney.eth')) {
    fullHandle += '.silamoney.eth';
  }
  return fullHandle;
};

exports.checkHandle = function (handle) {
  return new Promise(function (resolve, reject) {
    template('header_msg').then(function (temp) {
      var fullHandle = getFullHandle(handle);
      var message = setHeaders(temp, fullHandle)
      message.header.user_handle = fullHandle;
      message.header.auth_handle = appHandle;
      message.header.reference = uuid4();
      message.header.created = Math.round(new Date() / 1000);

      var opts = {
        uri: url('check_handle'),
        json: true,
        body: message,
        headers: {
          authsignature: sign(JSON.stringify(message), appKey)
        }
      };

      post(opts).then(function (res) {
        return resolve(res);
      }).catch(function (err) {
        return reject(err);
      });
    }).catch(function (err) {
      reject(err);
    });
  });
};

exports.register = function (data) {
  var user = data;

  var promise = new Promise(function (resolve, reject) {
    template('entity_msg').then(function (temp) {
      var handle = getFullHandle(user.handle);
      var message = setHeaders(temp, handle)

      message.header.user_handle = handle;
      message.header.auth_handle = appHandle;

      message.address.city = user.city;
      message.address.postal_code = user.zip;
      message.address.state = user.state;
      message.address.street_address_1 = user.address;

      message.contact.phone = user.phone;
      message.contact.email = user.email;

      message.crypto_entry.crypto_address = user.crypto;

      message.entity.birthdate = user.dob;
      message.entity.first_name = user.first_name;
      message.entity.last_name = user.last_name;
      message.entity.entity_name = user.first_name + ' ' + user.last_name;
      message.entity.relationship = 'user';

      message.identity.identity_value = user.ssn;

      var opts = {
        uri: url('register'),
        json: true,
        body: message,
        headers: {
          authsignature: sign(JSON.stringify(message), appKey)
        }
      };

      post(opts).then(function (res2) {
        return resolve(res2);
      }).catch(function (err) {
        return reject(err);
      });
    }).catch(function (err) {
      reject(err);
    });
  });
  return promise;
};

exports.requestKYC = function (data, key) {
  var user = data;

  var promise = new Promise(function (resolve, reject) {
    template('header_msg').then(function (temp) {
      var handle = getFullHandle(user.handle);
      var message = setHeaders(temp, handle);

      message.header.user_handle = handle;
      message.header.auth_handle = appHandle;

      var opts = {
        uri: url('request_kyc'),
        json: true,
        body: message,
        headers: {
          authsignature: sign(JSON.stringify(message), appKey),
          usersignature: sign(JSON.stringify(message), key)
        }
      };

      post(opts).then(function (res2) {
        return resolve(res2);
      }).catch(function (err) {
        return reject(err);
      });
    }).catch(function (err) {
      reject(err);
    });
  });
  return promise;
};

exports.checkKYC = function (handle, key) {
  var promise = new Promise(function (resolve, reject) {
    template('header_msg').then(function (temp) {
      var fullHandle = getFullHandle(handle);
      var message = setHeaders(temp, fullHandle);

      var opts = signOpts({
        uri: url('check_kyc'),
        json: true,
        body: message
      }, key);

      post(opts).then(function (res) {
        return resolve(res);
      }).catch(function (err) {
        return reject(err);
      });
    }).catch(function (err) {
      reject(err);
    });
  });
  return promise;
};

exports.linkAccount = function (handle, key, publicToken, accountName) {
  var promise = new Promise(function (resolve, reject) {
    template('link_account_msg').then(function (temp) {
      var fullHandle = getFullHandle(handle);
      var message = setHeaders(temp, fullHandle);
      message.public_token = publicToken;
      //MARK: Added to support multiple bank accounts
      if (accountName !== undefined){
        message.account_name = accountName
      } else {
        throw Error("Must specify an account name")
      }

      var opts = signOpts({
        uri: url('link_account'),
        json: true,
        body: message
      }, key);

      post(opts).then(function (res) {
        return resolve(res);
      }).catch(function (err) {
        return reject(err);
      });
    }).catch(function (err) {
      reject(err);
    });
  });
  return promise;
};

exports.issueSila = function (amount, handle, key, accountName) {
  var promise = new Promise(function (resolve, reject) {
    template('issue_msg').then(function (temp) {
      var fullHandle = getFullHandle(handle);
      var message = setHeaders(temp, fullHandle);
      message.amount = amount;
      //MARK: Added to support multiple bank accounts
      if (accountName !== undefined){
        message.account_name = accountName
      } else {
        throw Error("Must specify an account name")
      }

      var opts = signOpts({
        uri: url('issue_sila'),
        json: true,
        body: message
      }, key);

      post(opts).then(function (res) {
        return resolve(res);
      }).catch(function (err) {
        return reject(err);
      });
    }).catch(function (err) {
      reject(err);
    });
  });
  return promise;
};

exports.redeemSila = function (amount, handle, key, accountName) {
  var promise = new Promise(function (resolve, reject) {
    template('redeem_msg').then(function (temp) {
      var fullHandle = getFullHandle(handle);
      var message = setHeaders(temp, fullHandle);
      message.amount = amount;
      //MARK: Added to support multiple bank accounts
      if (accountName !== undefined) {
        message.account_name = accountName
      } else {
        throw Error("Must specify an account name")
      }

      var opts = signOpts({
        uri: url('redeem_sila'),
        json: true,
        body: message
      }, key);

      post(opts).then(function (res) {
        return resolve(res);
      }).catch(function (err) {
        return reject(err);
      });
    }).catch(function (err) {
      reject(err);
    });
  });
  return promise;
};

exports.transferSila = function (amount, handle, key, destination) {
  var promise = new Promise(function (resolve, reject) {
    template('transfer_msg').then(function (temp) {
      var fullHandle = getFullHandle(handle);
      var fullDestination = getFullHandle(destination);
      var message = setHeaders(temp, fullHandle);
      message.amount = amount;
      message.destination = fullDestination;

      var opts = signOpts({
        uri: url('transfer_sila'),
        json: true,
        body: message
      }, key);

      post(opts).then(function (res) {
        return resolve(res);
      }).catch(function (err) {
        return reject(err);
      });
    }).catch(function (err) {
      reject(err);
    });
  });
  return promise;
};

exports.getAccounts = function (handle, key) {
  var promise = new Promise(function (resolve, reject) {
    template('get_accounts_msg').then(function (temp) {
      var fullHandle = getFullHandle(handle);
      var message = setHeaders(temp, fullHandle);

      var opts = signOpts({
        uri: url('get_accounts'),
        json: true,
        body: message
      }, key);

      post(opts).then(function (res) {
        return resolve(res);
      }).catch(function (err) {
        return reject(err);
      });
    }).catch(function (err) {
      reject(err);
    });
  });
  return promise;
};

exports.getTransactions = function (handle, key) {
  var filters = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};

  var promise = new Promise(function (resolve, reject) {
    template('header_msg').then(function (temp) {
      var fullHandle = getFullHandle(handle);

      var message = setHeaders(temp, fullHandle);
      message.message = 'get_transactions_msg';

      message.search_filters = filters;

      var opts = signOpts({
        uri: url('get_transactions'),
        json: true,
        body: message
      }, key);

      post(opts).then(function (res) {
        return resolve(res);
      }).catch(function (err) {
        return reject(err);
      });
    }).catch(function (err) {
      reject(err);
    });
  });
  return promise;
};

exports.getBalance = function (address) {
  var promise = new Promise(function (resolve, reject) {
    var message = { address: address };

    var opts = {
      uri: getBalanceURL(),
      json: true,
      body: message
    };

    post(opts).then(function (res) {
      return resolve(res);
    }).catch(function (err) {
      return reject(err);
    });
  });
  return promise;
};

exports.addCrypto = function (handle, key, address, alias) {
  var promise = new Promise(function (resolve, reject) {
    template('crypto_msg').then(function (temp) {
      var fullHandle = getFullHandle(handle);
      var message = setHeaders(temp, fullHandle);
      message.crypto_entry.crypto_alias = alias;
      message.crypto_entry.crypto_address = address;

      var opts = signOpts({
        uri: url('add_crypto'),
        json: true,
        body: message
      }, key);

      post(opts).then(function (res) {
        return resolve(res);
      }).catch(function (err) {
        return reject(err);
      });
    }).catch(function (err) {
      reject(err);
    });
  });
  return promise;
};

exports.checkTransaction = function (id) {
  return id;
};
exports.configure = function (params) {
  appKey = params.key;appHandle = params.handle;
};
exports.setEnvironment = function (envString) {
  env = envString.toUpperCase();configureUrl();console.log('Setting environment to ' + envString.toUpperCase() + ': ' + baseUrl);
};
exports.enableSandbox = function () {
  sandbox = true;configureUrl();
};
exports.disableSandbox = function () {
  sandbox = false;configureUrl();
};
exports.generateWallet = function () {
  return web3.eth.accounts.create();
};
exports.setLogging = function (log) {
  logging = !!log;
};
